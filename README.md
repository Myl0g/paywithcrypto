# PayWithCrypto

Allows a person collecting a lot of payments from multiple different people to generate unique crypto payment addresses, monitor those addresses for incoming payments, and cash out those payments to addresses within the payee's direct control.

## Installation

```bash
go get gitlab.com/myl0g/paywithcrypto
```

## Usage

```bash
paywithcrypto [command] [arguments]
```
