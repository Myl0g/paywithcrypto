package config

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gitlab.com/myl0g/paywithcrypto/definitions"
)

// GetCurrencyConfig retrieves the configuration for the currency specified by its name.
func GetCurrencyConfig(name string) (definitions.CryptocurrencyConfig, error) {
	configs, err := getAllCurrencyConfigs()
	if err != nil {
		return definitions.CryptocurrencyConfig{}, err
	}

	var currentConf definitions.CryptocurrencyConfig
	for _, conf := range configs {
		if conf.Name == name {
			currentConf = conf
			break
		}
	}

	return currentConf, nil
}

// GetCashoutAddresses retrieves the configured cashout addresses.
func GetCashoutAddresses() (map[string]string, error) {
	file, _ := os.Open(GetConfigDirectory() + "cashoutaddresses.json")
	defer file.Close()
	bytes, _ := ioutil.ReadAll(file)

	var addresses map[string]string
	err := json.Unmarshal(bytes, &addresses)
	if err != nil {
		return nil, err
	}

	return addresses, nil
}

// GetCryptoAddresses retrieves the generated cryptocurrency addresses, disregarding the kind of currency.
func GetCryptoAddresses() ([]definitions.CryptoAddress, error) {
	file, _ := os.Open(GetConfigDirectory() + "addresses.json")
	defer file.Close()
	bytes, _ := ioutil.ReadAll(file)

	var addresses []definitions.CryptoAddress
	err := json.Unmarshal(bytes, &addresses)
	if err != nil {
		return nil, err
	}

	return addresses, nil
}

// SaveJSON marshalls the given data "v" and saves it to the path.
func SaveJSON(v interface{}, path string) error {
	bytes, err := json.MarshalIndent(v, "", "	")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(path, bytes, 0644)
	return err
}

func getAllCurrencyConfigs() ([]definitions.CryptocurrencyConfig, error) {
	jsonFile, err := os.Open(GetConfigDirectory() + "currencies.json")
	if err != nil {
		if os.IsNotExist(err) {
			file, _ := os.Create(GetConfigDirectory() + "currencies.json")
			file.Close()
			return getAllCurrencyConfigs()
		}
		return nil, err
	}
	defer jsonFile.Close()

	bytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	var config []definitions.CryptocurrencyConfig
	err = json.Unmarshal(bytes, &config)
	if err != nil {
		if err.Error() == "unexpected end of JSON input" {
			return make([]definitions.CryptocurrencyConfig, 0), nil
		}
		return nil, err
	}

	return config, nil
}
