package config

import (
	"encoding/json"
	"os"
	"time"

	"gitlab.com/myl0g/paywithcrypto/definitions"
)

// Setup checks to see if the configuration directory and files exist. If they do not, they are created and populated with sample data.
// If they do, nothing happens.
// Return of (true, nil) indicates that the config directory and sample files were created, (false, nil) indicates that setup had already occurred
// and nothing happened, (false, err) indicates that something went wrong and the process could not complete successfully.
func Setup() (bool, error) {
	var changed bool
	if _, err := os.Stat(GetConfigDirectory()); os.IsNotExist(err) {
		changed = true
		err = os.Mkdir(GetConfigDirectory(), 0755)
		if err != nil {
			return false, err
		}
	}

	if _, err := os.Stat(GetConfigDirectory() + "addresses.json"); os.IsNotExist(err) {
		changed = true
		file, err := os.Create(GetConfigDirectory() + "addresses.json")
		if err != nil {
			return false, err
		}

		bytes, _ := json.MarshalIndent([]definitions.CryptoAddress{{CurrencyName: "bitcoin", Address: "bc1qt5y0yspjm099rua6ukg8axnrwd45c7gy5mqp0x", CreationDate: time.Now(), RequestedAmount: 0.0, PaidAmount: 0.0, InvoiceNumber: 0}}, "", "	")
		file.Write(bytes)
		file.Close()
	}

	if _, err := os.Stat(GetConfigDirectory() + "cashoutaddresses.json"); os.IsNotExist(err) {
		changed = true
		file, err := os.Create(GetConfigDirectory() + "cashoutaddresses.json")
		if err != nil {
			return false, err
		}

		bytes, _ := json.MarshalIndent([]map[string]string{{"bitcoin": "xxxyyyzzz"}}, "", "	")
		file.Write(bytes)
		file.Close()
	}

	if _, err := os.Stat(GetConfigDirectory() + "currencies.json"); os.IsNotExist(err) {
		changed = true
		file, err := os.Create(GetConfigDirectory() + "currencies.json")
		if err != nil {
			return false, err
		}

		bytes, _ := json.MarshalIndent([]definitions.CryptocurrencyConfig{{Name: "bitcoin", GenerateAddressCmd: "electrum createnewaddress --offline", SendAllCmd: "electrum daemon -d; electrum payto -F {from} {to} !", GetBalanceCmd: "electrum daemon -d; electrum getaddressbalance {address} | jq '.confirmed'"}}, "", "	")
		file.Write(bytes)
		file.Close()
	}

	return changed, nil
}
