package config

import (
	"os"
	"runtime"
)

// GetConfigDirectory retrieves the path of the paywithcrypto configuration based on the current operating system.
func GetConfigDirectory() string {
	if runtime.GOOS == "windows" {
		return getUserConfigDirectory() + "paywithcrypto\\"
	}
	return getUserConfigDirectory() + "paywithcrypto/"
}

func getUserConfigDirectory() string {
	switch runtime.GOOS {
	case "windows":
		return os.Getenv("APPDATA") + "\\"
	case "darwin":
		return os.Getenv("HOME") + "/Library/Application Support/"
	case "linux":
		return os.Getenv("HOME") + "/.config/"
	}
	return ""
}
