package wallet

import (
	"os/exec"
	"strings"

	"gitlab.com/myl0g/paywithcrypto/config"
	"gitlab.com/myl0g/paywithcrypto/definitions"
)

// SendAll sends all coins from the provided address/private key to the given address.
func SendAll(from definitions.CryptoAddress, toAddress string) error {
	conf, err := config.GetCurrencyConfig(from.CurrencyName)
	if err != nil {
		return err
	}

	resolvedCmd := strings.ReplaceAll(strings.ReplaceAll(conf.SendAllCmd, "{to}", toAddress), "{from}", from.Address)
	arr := strings.Split(resolvedCmd, " ")
	_, err = exec.Command(arr[0], arr[1:]...).Output()
	return err
}
