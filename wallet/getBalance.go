package wallet

import (
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/myl0g/paywithcrypto/config"
	"gitlab.com/myl0g/paywithcrypto/definitions"
)

// GetBalance returns the given address's balance as recorded on the blockchain explorer for the proper currency.
func GetBalance(a definitions.CryptoAddress) (float64, error) {
	conf, err := config.GetCurrencyConfig(a.CurrencyName)
	if err != nil {
		return 0, err
	}

	resolvedCmd := strings.ReplaceAll(conf.GetBalanceCmd, "{address}", a.Address)
	arr := strings.Split(resolvedCmd, " ")
	output, err := exec.Command(arr[0], arr[1:]...).Output()
	if err != nil {
		return 0, err
	}

	float, err := strconv.ParseFloat(string(output[:]), 64)
	if err != nil {
		return 0, err
	}

	return float, nil
}
