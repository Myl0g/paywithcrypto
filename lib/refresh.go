package lib

import (
	"time"

	"gitlab.com/myl0g/paywithcrypto/config"
	"gitlab.com/myl0g/paywithcrypto/definitions"
	"gitlab.com/myl0g/paywithcrypto/wallet"
)

// Refresh examines all stored CryptoAddresses. Those which are unpaid and >5 hours old are purged, all others have their paid amounts updated.
func Refresh() error {
	stored, err := config.GetCryptoAddresses()
	if err != nil {
		return err
	}

	updated := make([]definitions.CryptoAddress, 0)
	for _, address := range stored {
		new, err := updateBalance(address)
		if err != nil {
			return err
		}

		maxTime, _ := time.ParseDuration("5h")
		if new.PaidAmount >= new.RequestedAmount || getAddressAge(address) < maxTime {
			updated = append(updated, new)
		}
	}

	return config.SaveJSON(updated, config.GetConfigDirectory()+"addresses.json")
}

func updateBalance(a definitions.CryptoAddress) (definitions.CryptoAddress, error) {
	newBalance, err := wallet.GetBalance(a)
	if err != nil {
		return definitions.CryptoAddress{}, err
	}
	a.PaidAmount = newBalance
	return a, nil
}

func getAddressAge(a definitions.CryptoAddress) time.Duration {
	return time.Since(a.CreationDate)
}
