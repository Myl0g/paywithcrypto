package lib

import (
	"gitlab.com/myl0g/paywithcrypto/config"
	"gitlab.com/myl0g/paywithcrypto/definitions"
	"gitlab.com/myl0g/paywithcrypto/wallet"
)

// Cashout moves all paid invoice balances to the central address for each currency (stipulated in the config) and returns the cashed addresses before discarding them from storage.
func Cashout() ([]definitions.CryptoAddress, error) {
	addresses, err := config.GetCryptoAddresses()
	if err != nil {
		return nil, err
	}

	cashedAddresses := []definitions.CryptoAddress{}

	for _, address := range addresses {
		cashedStatus, err := cashAddress(address)
		if err != nil {
			return nil, err
		}
		if cashedStatus {
			cashedAddresses = append(cashedAddresses, address)
		}
	}

	return cashedAddresses, nil
}

// Returns (true, nil) if address successfully cashed, (false, nil) if address unpaid, (false, err) if error occured
func cashAddress(a definitions.CryptoAddress) (bool, error) {
	cashouts, err := config.GetCashoutAddresses()
	if err != nil {
		return false, err
	}

	if a.RequestedAmount == a.PaidAmount {
		err = wallet.SendAll(a, cashouts[a.CurrencyName])
		if err != nil {
			return false, err
		}

		return true, nil
	}
	return false, nil
}
